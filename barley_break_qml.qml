import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4

Item {
    width: 200;
    height: 250

    property int cellsNumber: 16
    property int zeroIndex: 14

    ListModel {
        id: cellsModel

        Component.onCompleted:  {
            for (var i = 1; i < cellsNumber - 1; ++i)
                append({"value": i});
            append({"value": 0});
            append({"value": 15});
        }
    }

    GridView {
        id: cellsGrid

        anchors.fill: parent
        cellWidth: parent.width / 4
        cellHeight: (parent.height - shuffleButton.height) / 4

        model: cellsModel

        delegate: Rectangle {
            id: rect

            width: parent.width / 4 - 5
            height: parent.height / 4 - 5
            color: value > 0 ? "skyblue" : "transparent"

            Text {
                anchors.centerIn: parent
                font.pixelSize: parent.height / 3
                text: value > 0 ? value : ""
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {

                    //check if possible to move to empty cell
                    var isCloseToZero = Math.abs(zeroIndex - index) === 4 ||
                            (Math.abs(zeroIndex - index) === 1 &&
                             Math.floor(zeroIndex / 4) === Math.floor(index / 4));
                    if (isCloseToZero)
                    {
                        moveCells(index, zeroIndex);

                        if (cellsInRightOrder())
                        {
                            cellsModel.setProperty(15, "value", 16);
                            messageWon.open();
                        }
                    }
                }
            }
        }
        move: Transition {
            NumberAnimation {properties: "x,y"; duration: 1000;}
        }

    }

    Button {
        id: shuffleButton

        height: 30
        anchors.bottom: parent.bottom
        text: "Shuffle"

        onClicked: {

            if (cellsModel.get(15).value === 16)
                cellsModel.setProperty(15, "value", 0);

            do {
                shuffleCells();
            } while(!checkSolving());
        }
    }

    MessageDialog {
        id: messageWon

        title: "Congratulations"
        text: "You've finished this game and all tiles are in right order:)"
    }

    function cellsInRightOrder() {
        var finished = true;

        for (var i = 0; i < cellsNumber - 1; i++)
        {
            if (cellsModel.get(i).value !== i + 1)
            {
                finished = false;
                break;
            }
        }
        return finished;
    }

    function moveCells(from, to)
    {
        cellsModel.move(from, to, 1)
        to += to <= from ? 1 : -1
        if (to >= 0 && to < 16)
            cellsModel.move(to, from, 1)
        if (cellsModel.get(from).value === 0)
            zeroIndex = from
    }

    function checkSolving()
    {
        var sum = 0;

        for (var i = 0; i < cellsNumber; ++i)
        {
            var number1 = cellsModel.get(i);

            if (number1 === 0)
            {
                sum += i % 4 + 1;
                continue;
            }
            for (var j = i + 1; j < cellsNumber; ++j)
            {
                var number2 = cellsModel.get(j);

                if (number2 < number1 && number2 !== 0)
                    ++sum;
            }
        }
        return (sum % 2 !== 1);
    }

    function shuffleCells()
    {
        for (var i = 16; i; --i)
        {
            var newPossition = Math.floor(Math.random() * i);
            moveCells(i - 1, newPossition);
        }
    }
}
